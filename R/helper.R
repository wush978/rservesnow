clEval <- function(expr) {
  expr <- substitute(expr)
  RS.eval(.RserveScupio$con, bquote(clusterEvalQ(.RserveScupio$cl, .(expr))), lazy=FALSE)
}